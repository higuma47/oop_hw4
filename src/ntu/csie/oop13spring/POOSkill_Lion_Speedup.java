package ntu.csie.oop13spring;
import java.util.*;

public class POOSkill_Lion_Speedup extends POOSkill{
	public void act(POOPet pet) {
		Random ran = new Random();
		int ori_AGI = pet.getAGI();
		int after_AGI = ori_AGI + 120;
		int rrr;
		if(after_AGI >= 1024) {
			after_AGI = 1023;
		}
		if(pet.setAGI(after_AGI)) {
			rrr = ran.nextInt(3);
			switch(rrr) {
				case 0:
					System.out.println("\033[1;32m+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\033[m");
					System.out.printf (" \033[1;35m%s:\033[m \033[1;37mHahaha !!(\033[m\033[1;33mNot Compile Error XD\033[m\033[1;37m)\033[m \033[1;37mI'm ligher and faster !!!\033[m   \n", pet.getName());
					System.out.println("\033[1;32m+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\033[m");
					break;
				case 1:
					System.out.println("\033[1;32m+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\033[m");
					System.out.printf (" \033[1;35m%s:\033[m \033[1;37mI'm stronger !!!!!!!\033[m  \n", pet.getName());
					System.out.println("\033[1;32m+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\033[m");
					break;
				case 2:
					System.out.println("\033[1;32m+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\033[m");
					System.out.printf (" \033[1;35m%s:\033[m \033[1;37m^___________________<\033[m  \n", pet.getName());
					System.out.println("\033[1;32m+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\033[m");
					break;
				default:
					break;
			}
			System.out.println("");
			System.out.println("");
			System.out.println("");
			System.out.printf("\033[1;33mYour AGI:\033[m \033[1;37m%d\033[m \033[1;33m->\033[m \033[1;36m%d\033[m\033[1;33m.\033[m\n", ori_AGI, after_AGI);
			System.out.println("");
			System.out.println("");
			System.out.println("Press Enter to continue...");
			try {
				System.in.read();
			}
			catch(Exception e) {
			}
		}
	}
}
