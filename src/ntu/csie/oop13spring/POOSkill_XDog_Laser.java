package ntu.csie.oop13spring;
import java.util.*;

public class POOSkill_XDog_Laser extends POOSkill{
	public void act(POOPet pet) {
		Random ran = new Random();
		int ori_HP = pet.getHP();
		int after_HP = ori_HP - 50;
		int rrr;
		if(after_HP < 0) {
			after_HP = 0;
		}
		if(pet.setHP(after_HP)) {
			rrr = ran.nextInt(3);
			switch(rrr) {
				case 0:
					System.out.println("\033[1;31m+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\033[m");
					System.out.printf (" \033[1;36m%s:\033[m \033[1;37mWTF !!!\033[m \033[1;33mQ_______Q\033[m  \n", pet.getName());
					System.out.println("\033[1;31m+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\033[m");
					break;
				case 1:System.out.println("\033[1;31m+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\033[m");
					System.out.printf (" \033[1;36m%s:\033[m \033[1;37mOh my God !!!!!\033[m  \n", pet.getName());
					System.out.println("\033[1;31m+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\033[m");
					break;
				case 2:
					System.out.println("\033[1;31m+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\033[m");
					System.out.printf (" \033[1;36m%s:\033[m \033[1;37mQ___________________Q\033[m  \n", pet.getName());
					System.out.println("\033[1;31m+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\033[m");
					break;
				default:
					break;
			}
			System.out.println("");
			System.out.println("");
			System.out.println("");
			System.out.printf("\033[1;33mYou give\033[m \033[1;36m%s\033[m \033[1;37m50\033[m \033[1;33mdamage. It remains \033[m\033[1;32m%d\033[m\033[1;33m HP now.\033[m\n", pet.getName(), after_HP);
			System.out.println("");
			System.out.println("");
			System.out.println("Press Enter to continue...");
			try {
				System.in.read();
			}
			catch(Exception e) {
			}
		}
	}
}
