package ntu.csie.oop13spring;
import java.util.*;

public class POOSkill_Lion_Concentrate extends POOSkill{
	public void act(POOPet pet) {
		Random ran = new Random();
		int ori_MP = pet.getMP();
		int after_MP = ori_MP + 10;
		int rrr;
		if(after_MP >= 1024) {
			after_MP = 1023;
		}
		if(pet.setMP(after_MP)) {
			rrr = ran.nextInt(3);
			switch(rrr) {
				case 0:
					System.out.println("\033[1;32m+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\033[m");
					System.out.printf (" \033[1;35m%s:\033[m \033[1;37mHahaha !!(\033[m\033[1;33mNot Compile Error XD\033[m\033[1;37m)\033[m \033[1;37mI feel I'm smarter now. ^_<\033[m   \n", pet.getName());
					System.out.println("\033[1;32m+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\033[m");
					break;
				case 1:
					System.out.println("\033[1;32m+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\033[m");
					System.out.printf (" \033[1;35m%s:\033[m \033[1;37mI'm stronger !!!!!!!\033[m  \n", pet.getName());
					System.out.println("\033[1;32m+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\033[m");
					break;
				case 2:
					System.out.println("\033[1;32m+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\033[m");
					System.out.printf (" \033[1;35m%s:\033[m \033[1;37m>___________________^\033[m  \n", pet.getName());
					System.out.println("\033[1;32m+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\033[m");
					break;
				default:
					break;
			}
			System.out.println("");
			System.out.println("");
			System.out.println("");
			System.out.printf("\033[1;33mYour MP:\033[m \033[1;37m%d\033[m \033[1;33m->\033[m \033[1;32m%d\033[m\033[1;33m.\033[m\n", ori_MP, after_MP);
			System.out.println("");
			System.out.println("");
			System.out.println("Press Enter to continue...");
			try {
				System.in.read();
			}
			catch(Exception e) {
			}
		}
	}
}
