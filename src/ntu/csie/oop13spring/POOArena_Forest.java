package ntu.csie.oop13spring;
import java.util.*;

class indexName {
	String name;
	int index;
}

public class POOArena_Forest extends POOArena {
	private int round;
	protected POOPet[] petArray;
	private indexName[][] forestMap;
	public POOArena_Forest() {
		forestMap = new indexName[21][];
		int i, j;
		for(i = 0; i < 21; i++) {
			forestMap[i] = new indexName[21];
		}
		for(i = 0; i < 21; i++) {
			for(j = 0; j < 21; j++) {
				forestMap[i][j] = new indexName();
			}
		}
		for(i = 0; i < 21; i++) {
			for(j = 0; j < 21; j++) {
				forestMap[i][j].index = 0;
				forestMap[i][j].name = null;
			}
		}
	}
	private boolean checkInit() {
		int i, j;
		for(i = 0; i < 21; i++) {
			for(j = 0; j < 21; j++) {
				if(forestMap[i][j].index != 0) {
					return false;
				}
			}
		}
		return true;
	}
	private void initMap() {
		int i, j;
		for(i = 0; i < 21; i++) {
			for(j = 0; j < 21; j++) {
				if(i % 4 == 0 && j % 4 == 0) {
					forestMap[i][j].name = "Tree";
					forestMap[i][j].index = -100;
				}
				else {
					forestMap[i][j].index = -1;
				}
			}
		}
		Random ran = new Random();
		int x, y;
		for(i = 0; i < petArray.length; i++) {
			x = ran.nextInt(21);
			y = ran.nextInt(21);
			if(forestMap[x][y].index == -1) {
				forestMap[x][y].name = petArray[i].getName();
				forestMap[x][y].index = i;
			}
			else {
				i--;
			}
		}
	}
	private void clearPetFromMap(POOPet p, int num) {
		int i, j;
		for(i = 0; i < 21; i++) {
			for(j = 0; j < 21; j++) {
				if(forestMap[i][j].index == num && forestMap[i][j].name.compareTo(p.getName()) == 0) {
					forestMap[i][j].name = null;
					forestMap[i][j].index = -1;
				}
			}
		}
	}
	private boolean checkRange(POOCoordinate now, POOCoordinate des, int range) {
		int res_x;
		int res_y;
		int res;
		if(now.x == des.x && now.y == des.y) {
			return true;
		}
		if(des.x >= 0 && des.x < 21 && des.y >= 0 && des.y < 21) {
			if((des.x - now.x) < 0) {
				res_x = (des.x - now.x) * (-1);
			}
			else {
				res_x = des.x - now.x;
			}
			if((des.y - now.y) < 0) {
				res_y = (des.y - now.y) * (-1);
			}
			else {
				res_y = des.y - now.y;
			}
			res = res_x + res_y;
			if(res > range) {
				return false;
			}
			else {
				if(forestMap[des.x][des.y].index == -1) {
					return true;
				}
				else {
					return false;
				}
			}
		}
		else {
			return false;
		}
	}
	public boolean isInteger(String s) {
		try {
			Integer.parseInt(s);
		}
		catch(NumberFormatException e) {
			return false;
		}
		// only got here if we didn't return false
		return true;
	}
	public boolean fight() {
		int i, j, k;
		if(this.checkInit()) {
			petArray = this.getAllPets();
			this.initMap();
			System.out.println("");
			System.out.println("\033[1;31m==\033[m\033[1;32m==\033[m\033[1;33m==\033[m\033[1;34m==\033[m\033[1;35m==\033[m\033[1;36m==\033[m\033[1;37m==\033[m\033[1;31m==\033[m\033[1;32m==\033[m\033[1;33m==\033[m\033[1;34m==\033[m\033[1;35m==\033[m\033[1;36m==\033[m\033[1;37m==\033[m\033[1;31m==\033[m\033[1;32m==\033[m\033[1;33m==\033[m\033[1;34m==\033[m\033[1;35m==\033[m\033[1;36m==\033[m\033[1;37m==\033[m");
			System.out.println(" \033[1;37mHello!! Welcome to this POOFight Game!!! \033[m");
			System.out.println("\033[1;37m==\033[m\033[1;36m==\033[m\033[1;35m==\033[m\033[1;34m==\033[m\033[1;33m==\033[m\033[1;32m==\033[m\033[1;31m==\033[m\033[1;37m==\033[m\033[1;36m==\033[m\033[1;35m==\033[m\033[1;34m==\033[m\033[1;33m==\033[m\033[1;32m==\033[m\033[1;31m==\033[m\033[1;37m==\033[m\033[1;36m==\033[m\033[1;35m==\033[m\033[1;34m==\033[m\033[1;33m==\033[m\033[1;32m==\033[m\033[1;31m==\033[m");
			System.out.println("");
		}
		round++;
		System.out.println("");
		System.out.printf("-- \033[1;37mRound %d\033[m --\n", round);
		System.out.println("");
		System.out.println("Press Enter to continue...");
		try {
			System.in.read();
		}
		catch(Exception e) {
		}
		System.out.println("");
		POOCoordinate coo;
		POOAction a;
		POOCoordinate now;
		boolean range_flag;
		int range;
		int command = 100;
		Scanner scanner = new Scanner(System.in);
		int move_count;
		int act_count;
		boolean end_round_flag;
		int count = 0;
		int now_HP;
		String tmp;

		for(i = 0; i < petArray.length; i++) {
			if(petArray[i].getHP() > 0) {
				now = this.getPosition(petArray[i]);
				range = petArray[i].getAGI() / 40;
				move_count = 1;
				act_count = 1;
				end_round_flag = false;
				while(true) {
					System.out.println("");
					System.out.println("");
					System.out.println("");
					System.out.println("");
					System.out.println("");
					System.out.println("\033[1;32m#####################################################################\033[m");
					System.out.printf ("\033[1;32m#\033[m    You are \033[1;33mPet(%d)\033[m \033[1;35m%s\033[m, what action do you want to do ?      \033[1;32m#\033[m\n", i, petArray[i].getName());
					System.out.println("\033[1;32m#\033[m                                                                   \033[1;32m#\033[m");
					System.out.println("\033[1;32m#\033[m    \033[1;33m(0)\033[m Watch the map.                                             \033[1;32m#\033[m");
					System.out.println("\033[1;32m#\033[m    \033[1;33m(1)\033[m Move.                                                      \033[1;32m#\033[m");
					System.out.println("\033[1;32m#\033[m    \033[1;33m(2)\033[m Use your skills.                                           \033[1;32m#\033[m");
					System.out.println("\033[1;32m#\033[m    \033[1;33m(3)\033[m End this round.                                            \033[1;32m#\033[m");
					System.out.println("\033[1;32m#\033[m    \033[1;33m(4)\033[m Show all pets status.                                      \033[1;32m#\033[m");
					System.out.println("\033[1;32m#####################################################################\033[m");
					System.out.println("");
					System.out.printf("\033[1;37mPlease enter an action number :\033[m ");

					tmp = scanner.next();
					scanner.nextLine();
					System.out.println("");
					if(isInteger(tmp)) {
						command = Integer.parseInt(tmp);
					}
					else {
						command = 100;
					}

					switch(command) {
						case 0:
							System.out.println("");
							System.out.println("\033[1;31m======================\033[m");
							System.out.println("");
							System.out.println("--- \033[1;37mArena Map\033[m ---");
							System.out.println("");
							this.showMap();
							System.out.println("");
							System.out.println("\033[1;31m======================\033[m");
							System.out.println("");
							System.out.println("Press Enter to continue...");
							try {
								System.in.read();
							}
							catch(Exception e) {
							}
							break;
						case 1:
							while(true) {
								if(move_count > 0) {
									System.out.println("");
									System.out.println("\033[1;31m======================\033[m");
									System.out.println("--- \033[1;37mArena Map\033[m ---");
									System.out.println("");
									this.showMoveMap(now.x, now.y, range);
									System.out.println("\033[1;31m======================\033[m");
									System.out.printf("-- \033[1;37mYour current position is \033[m\033[1;36m(%d, %d)\033[m\033[1;37m.\033[m\n", now.x, now.y);
									System.out.printf("-- \033[1;37mYour AGI is \033[m\033[1;35m%d\033[m \033[1;37mand you can move\033[m \033[1;35m%d\033[m \033[1;37mfrom here.\033[m\n", petArray[i].getAGI(), range);
									System.out.println("-- \033[1;33mWhere do you want to move to ?\033[m");
									System.out.println("-- \033[1;37mEx1: Enter \"\033[m\033[1;32m2 3\033[m\033[1;37m\" means move up \033[m\033[1;32m2\033[m\033[1;37m and move right \033[m\033[1;32m3\033[m\033[1;37m.\033[m");
									System.out.println("-- \033[1;37mEx2: Enter \"\033[m\033[1;32m-1 -2\033[m\033[1;37m\" means move down\033[m \033[1;32m1\033[m\033[1;37m and move left \033[m\033[1;32m2\033[m\033[1;37m.\033[m");
									coo = petArray[i].move(this);
									if(coo == null) {
										System.out.println("");
										System.out.println("");
										System.out.println("Error Input!!!");
										System.out.println("Press Enter to continue...");
										try {
											System.in.read();
										}
										catch(Exception e) {
										}
										continue;
									}
									coo.x = now.x - coo.x;
									coo.y = now.y + coo.y;
									range_flag = checkRange(now, coo, range);
									if(range_flag == false) {
										System.out.println("");
										System.out.println("");
										System.out.println("Error: Move out of range or it already exists an object on this coordinate.");
										System.out.println("Press Enter to continue...");
										try {
											System.in.read();
										}
										catch(Exception e) {
										}
										continue;
									}
									if(now.x != coo.x || now.y != coo.y) {
										forestMap[ coo.x ][ coo.y ].index = i;
										forestMap[ coo.x ][ coo.y ].name = forestMap[ now.x ][ now.y ].name;
										forestMap[ now.x ][ now.y ].index = -1;
										forestMap[ now.x ][ now.y ].name = null;
									}
									move_count--;
									System.out.println("");
									System.out.println("\033[1;31m======================\033[m");
									System.out.println("");
									System.out.println("--- \033[1;37mArena Map\033[m ---");
									System.out.println("");
									this.showMap();
									System.out.println("");
									System.out.println("\033[1;31m======================\033[m");
									System.out.println("");
									System.out.println("Press Enter to continue...");
									try {
										System.in.read();
									}
									catch(Exception e) {
									}	
									break;
								}
								else {
									System.out.println("Error : You can only move 1 time in a round.");
									System.out.println("Press Enter to continue...");
									try {
										System.in.read();
									}
									catch(Exception e) {
									}
									break;
								}
							}
							break;
						case 2:
							if(act_count > 0) {
								a = petArray[i].act(this);
								a.skill.act(a.dest);
								range = petArray[i].getAGI() / 40;
								act_count--;
							}
							else {
								System.out.println("Error : You can only act 1 time in a round.");
								System.out.println("Press Enter to continue...");
								try {
									System.in.read();
								}
								catch(Exception e) {
								}
								break;
							}
							break;
						case 3:
							end_round_flag = true;
							break;
						case 4:
							System.out.println("\033[1;31m===================================\033[m");
							System.out.println("");
							this.showPetStatus();
							System.out.println("");
							System.out.println("\033[1;31m===================================\033[m");
							System.out.println("");
							System.out.println("Press Enter to continue...");
							try {
								System.in.read();
							}
							catch(Exception e) {
							}
							break;
						default:
							System.out.println("Error command!!!");
							System.out.println("Press Enter to continue...");
							try {
								System.in.read();
							}
							catch(Exception e) {
							}
							break;
					}
					if(end_round_flag == true) {
						break;
					}
				}
			}
			// Check whether the pets die or not.
			count = 0;
			for(j = 0; j < petArray.length; j++) {
				now_HP = petArray[j].getHP();
				if(now_HP <= 0) {
					this.clearPetFromMap(petArray[j], j);
				}
				else {
					count++;
				}
			}
			System.out.printf("\033[1;35m%s:\033[m End Round.\n", petArray[i].getName());
			System.out.println("Press Enter to continue...");
			try {
				System.in.read();
			}
			catch(Exception e) {
			}
		}

		// If there is less than one pets alive, then end this game.
		if(count <= 1) {
			for(j = 0; j < petArray.length; j++) {
				now_HP = petArray[j].getHP();
				if(now_HP > 0) {
					System.out.println("");
					System.out.println("");
					System.out.println("\033[1;31m==\033[m\033[1;32m==\033[m\033[1;33m==\033[m\033[1;34m==\033[m\033[1;35m==\033[m\033[1;36m==\033[m\033[1;37m==\033[m\033[1;31m==\033[m\033[1;32m==\033[m\033[1;33m==\033[m\033[1;34m==\033[m\033[1;35m==\033[m\033[1;36m==\033[m\033[1;37m==\033[m\033[1;31m==\033[m\033[1;32m==\033[m\033[1;33m==\033[m\033[1;34m==\033[m\033[1;35m==\033[m\033[1;36m==\033[m\033[1;37m==\033[m");
					System.out.printf("        \033[1;37m%s Win !!!!!!!!!!!!\033[m\n", petArray[j].getName());
					System.out.println("\033[1;37m==\033[m\033[1;36m==\033[m\033[1;35m==\033[m\033[1;34m==\033[m\033[1;33m==\033[m\033[1;32m==\033[m\033[1;31m==\033[m\033[1;37m==\033[m\033[1;36m==\033[m\033[1;35m==\033[m\033[1;34m==\033[m\033[1;33m==\033[m\033[1;32m==\033[m\033[1;31m==\033[m\033[1;37m==\033[m\033[1;36m==\033[m\033[1;35m==\033[m\033[1;34m==\033[m\033[1;33m==\033[m\033[1;32m==\033[m\033[1;31m==\033[m");
					System.out.println("");
					System.out.println("<< Press Enter to End The Game. >>");
					try {
						System.in.read();
					}
					catch(Exception e) {
					}
				}
			}
			return false;
		}
		else {
			return true;
		}
	}
	private void showMap() {
		int i, j;
		for(i = 0; i < 21; i++) {
			for(j = 0; j < 21; j++) {
				if(forestMap[i][j].index == -1) {
					System.out.printf("\033[1;37m.\033[m");
				}
				else if(forestMap[i][j].index == -100) {
					System.out.printf("\033[1;32mT\033[m");
				}
				else {
					System.out.printf("\033[1;35m%d\033[m", forestMap[i][j].index);
				}
			}
			System.out.printf("\n");
		}
	}
	private void showMoveMap(int x, int y, int range) {
		int i, j;
		int dis_x, dis_y;
		for(i = 0; i < 21; i++) {
			for(j = 0; j < 21; j++) {
				if(forestMap[i][j].index == -1) {
					if(i - x < 0) {
						dis_x = x - i;
					}
					else {
						dis_x = i - x;
					}
					if(j - y < 0) {
						dis_y = y - j;
					}
					else {
						dis_y = j - y;
					}
					if(dis_x + dis_y <= range) {
						System.out.printf("\033[1;33m*\033[m");
					}
					else {
						System.out.printf("\033[1;37m.\033[m");
					}
				}
				else if(forestMap[i][j].index == -100) {
					System.out.printf("\033[1;32mT\033[m");
				}
				else {
					System.out.printf("\033[1;35m%d\033[m", forestMap[i][j].index);
				}
			}
			System.out.printf("\n");
		}
	}
	private void showPetStatus() {
		int i;
		POOCoordinate coo;
		for(i = 0; i < petArray.length; i++) {
			coo = this.getPosition(petArray[i]);
			if(coo != null) {
				System.out.printf("\033[1;33mPet(%d)\033[m\t\033[1;35m%s\033[m\n", i, petArray[i].getName());
				System.out.printf("\t\033[1;37mat position\033[m \033[1;36m(%d, %d)\033[m\n", coo.x, coo.y);
				System.out.printf("\t\033[1;37mHP:\033[m \033[1;33m%d\033[m  \033[1;37mMP:\033[m \033[1;32m%d\033[m  \033[1;37mAGI:\033[m \033[1;36m%d\033[m\n", petArray[i].getHP(), petArray[i].getMP(), petArray[i].getAGI());
				System.out.println("");
			}
		}
	}
	public void show() {
		System.out.println("");
		System.out.println("");
		System.out.println("\033[1;32m##################################\033[m");
		System.out.println("\033[1;32m#\033[m        \033[1;37mAfter this round\033[m        \033[1;32m#\033[m");
		System.out.println("\033[1;32m##################################\033[m");
		
		System.out.println("");

		this.showPetStatus();
		System.out.println("");
		System.out.println("");
		System.out.println("\033[1;31m===========================================\033[m");
		System.out.println("");
		System.out.println("");

	}
	public POOCoordinate getPosition(POOPet p) {
		int i, j;
		POOCoo coo = new POOCoo();
		for(i = 0; i < 21; i++) {
			for(j = 0; j < 21; j++) {
				if(forestMap[i][j].name != null) {
					if(forestMap[i][j].name.compareTo(p.getName()) == 0) {
						coo.x = i;
						coo.y = j;
						return (POOCoordinate)coo;
					}
				}
			}
		}
		return null;
	}
}
