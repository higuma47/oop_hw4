package ntu.csie.oop13spring;
import java.util.*;
public class POOPet_XDog extends POOPet {
	public static int count = 0;
	public POOPet_XDog() {
		this.setHP(400);
		this.setMP(250);
		this.setAGI(240);
		String name = "POOXDog_" + count;
		this.setName(name);
		count++;
	}
	protected POOAction act(POOArena arena) {
		Scanner scanner = new Scanner(System.in);
		POOArena_Forest arena_f = (POOArena_Forest) arena;
		int hp = this.getHP();
		int mp = this.getMP();
		int agi = this.getAGI();
		POOAction a = new POOAction();
		POOSkill_XDog_Bite skill_1 = new POOSkill_XDog_Bite();
		POOSkill_XDog_Laser skill_2 = new POOSkill_XDog_Laser();
		POOSkill_XDog_Hahaha skill_3 = new POOSkill_XDog_Hahaha();
		POOSkill_XDog_Concentrate skill_4 = new POOSkill_XDog_Concentrate();
		int command;
		String tmp;
		int des_pet;
		POOPet p;
		POOCoordinate des_coo;
		POOCoordinate my_coo;
		int res_x, res_y, res;
		Random ran = new Random();
		int rrr;
		while(true) {
			System.out.println("");
			System.out.println("");
			System.out.println("");
			System.out.println("");
			System.out.println("");
			System.out.println("\033[1;31m========================================================\033[m");
			System.out.println("\033[1;37mYour status: \033[m");
			System.out.printf("\033[1;37mHP:\033[m \033[1;33m%d\033[m\033[1;37m, MP:\033[m \033[1;32m%d\033[m\033[1;37m, AGI:\033[m \033[1;36m%d\033[m\n", hp, mp, agi);
			System.out.println("");
			System.out.println("\033[1;37m--------------------------------------------------------\033[m");
			System.out.println("");
			System.out.println("\033[1;33mWhat skill would you like to use ?\033[m");
			System.out.println("\033[1;35m(0)\033[m Bite Attack  (\033[1;31m-20\033[m MP, Damage \033[1;32m100\033[m, Attack Range: \033[1;36m1\033[m)");
			System.out.println("\033[1;35m(1)\033[m Laser Attack (\033[1;31m-30\033[m MP, Damage \033[1;32m50\033[m , Attack Range: \033[1;36m7\033[m)");
			System.out.println("\033[1;35m(2)\033[m Hahaha       (\033[1;31m-30\033[m MP, Damage \033[1;32m50\033[m HP and \033[1;33m10\033[m MP, Attack Range: \033[1;36m2\033[m)");
			System.out.println("\033[1;35m(3)\033[m Concentrate  (\033[1;32m+10\033[m MP)");
			System.out.println("\033[1;31m========================================================\033[m");
			System.out.println("");
			System.out.printf("\033[1;37mPlease enter the skill you want to use : \033[m");

			tmp = scanner.next();
			scanner.nextLine();
			System.out.println("");
			if(isInteger(tmp)) {
				command = Integer.parseInt(tmp);
			}
			else {
				command = 100;
			}

			switch(command) {
				case 0:
					if(mp < 20) {
						System.out.println("");
						System.out.println("MP not enough !!!");
						System.out.println("Press Enter to continue...");
						try {
							System.in.read();
						}
						catch(Exception e) {
						}
					}
					else {
						System.out.println("\033[1;33mWhich pet you'd like to attack ?\033[m");
						System.out.println("");
						System.out.printf("\033[1;37mPlease enter the pet index : \033[m");
						tmp = scanner.next();
						scanner.nextLine();
						System.out.println("");
						if(isInteger(tmp)) {
							des_pet = Integer.parseInt(tmp);
						}
						else {
							System.out.println("");
							System.out.println("Error Input!!!");
							System.out.println("Press Enter to continue...");
							try {
								System.in.read();
							}
							catch(Exception e) {
							}
							break;
						}
						if(des_pet >= 0 && des_pet < arena_f.petArray.length) {
							if(arena_f.petArray[des_pet] == null) {
								System.out.println("");
								System.out.println("Error : This pet is already dead!!!");
								System.out.println("Press Enter to continue...");
								try {
									System.in.read();
								}
								catch(Exception e) {
								}
							}
							else {
								p = arena_f.petArray[des_pet];
								des_coo = arena.getPosition(p);
								if(des_coo == null) {
									System.out.println("");
									System.out.println("Error : This pet is already dead!!!");
									System.out.println("Press Enter to continue...");
									try {
										System.in.read();
									}
									catch(Exception e) {
									}
								}
								else {
									my_coo = arena.getPosition(this);
									if(des_coo.x - my_coo.x < 0) {
										res_x = (des_coo.x - my_coo.x) * (-1);
									}
									else {
										res_x = des_coo.x - my_coo.x;
									}
									if(des_coo.y - my_coo.y < 0) {
										res_y = (des_coo.y - my_coo.y) * (-1);
									}
									else {
										res_y = des_coo.y - my_coo.y;
									}
									res = res_x + res_y;
									if(res <= 1) {
										this.setMP(mp - 20);
										a.skill = skill_1;
										a.dest = p;
										System.out.println("");
										System.out.println("");
										System.out.println("");
										rrr = ran.nextInt(3);
										switch(rrr) {
											case 0:
												System.out.println("\033[1;32m+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\033[m");
												System.out.printf (" \033[1;35m%s:\033[m \033[1;37mHahaha!!! Bite You!!!\033[m  \n", this.getName());
												System.out.println("\033[1;32m+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\033[m");
												System.out.println("");
												break;
											case 1:
												System.out.println("\033[1;32m+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\033[m");
												System.out.printf (" \033[1;35m%s:\033[m \033[1;37mXDDDDDDDDDDDDDDDDDD\033[m  \n", this.getName());
												System.out.println("\033[1;32m+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\033[m");
												System.out.println("");
												break;
											case 2:
												System.out.println("\033[1;32m+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\033[m");
												System.out.printf (" \033[1;35m%s:\033[m \033[1;37mI'm hungry. I want to eat\033[m \033[1;31msomething\033[m\033[1;37m!!\033[m  \n", this.getName());
												System.out.println("\033[1;32m+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\033[m");
												System.out.println("");
												break;
											default:
												break;
										}
										return a;
									}
									else {
										System.out.println("");
										System.out.println("Error : Out of range!!!");
										System.out.println("Press Enter to continue...");
										try {
											System.in.read();
										}
										catch(Exception e) {
										}
										break;
									}
								}
							}
						}
						else {
							System.out.println("");
							System.out.println("Error : Pet doesn't exist!!");
							System.out.println("Press Enter to continue...");
							try {
								System.in.read();
							}
							catch(Exception e) {
							}
						}
					}
					break;
				case 1:
					if(mp < 30) {
						System.out.println("");
						System.out.println("MP not enough !!!");
						System.out.println("Press Enter to continue...");
						try {
							System.in.read();
						}
						catch(Exception e) {
						}
					}
					else {
						System.out.println("\033[1;33mWhich pet you'd like to attack ?\033[m");
						System.out.println("");
						System.out.printf("\033[1;37mPlease enter the pet index : \033[m");
						tmp = scanner.next();
						scanner.nextLine();
						System.out.println("");
						if(isInteger(tmp)) {
							des_pet = Integer.parseInt(tmp);
						}
						else {
							System.out.println("");
							System.out.println("Error Input!!!");
							System.out.println("Press Enter to continue...");
							try {
								System.in.read();
							}
							catch(Exception e) {
							}
							break;
						}
						if(des_pet >= 0 && des_pet < arena_f.petArray.length) {
							if(arena_f.petArray[des_pet] == null) {
								System.out.println("");
								System.out.println("Error : This pet is already dead!!!");
								System.out.println("Press Enter to continue...");
								try {
									System.in.read();
								}
								catch(Exception e) {
								}
							}
							else {
								p = arena_f.petArray[des_pet];
								des_coo = arena.getPosition(p);
								if(des_coo == null) {
									System.out.println("");
									System.out.println("Error : This pet is already dead!!!");
									System.out.println("Press Enter to continue...");
									try {
										System.in.read();
									}
									catch(Exception e) {
									}
								}
								else {
									my_coo = arena.getPosition(this);
									if(des_coo.x - my_coo.x < 0) {
										res_x = (des_coo.x - my_coo.x) * (-1);
									}
									else {
										res_x = des_coo.x - my_coo.x;
									}
									if(des_coo.y - my_coo.y < 0) {
										res_y = (des_coo.y - my_coo.y) * (-1);
									}
									else {
										res_y = des_coo.y - my_coo.y;
									}
									res = res_x + res_y;
									if(res <= 7) {
										this.setMP(mp - 30);
										a.skill = skill_2;
										a.dest = p;
										System.out.println("");
										System.out.println("");
										System.out.println("");
										rrr = ran.nextInt(3);
										switch(rrr) {
											case 0:
												System.out.println("\033[1;32m+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\033[m");
												System.out.printf (" \033[1;35m%s:\033[m \033[1;37mShining Day!!!!\033[m  \n", this.getName());
												System.out.println("\033[1;32m+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\033[m");
												System.out.println("");
												break;
											case 1:
												System.out.println("\033[1;32m+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\033[m");
												System.out.printf (" \033[1;35m%s:\033[m \033[1;37mXDDDDDDDDDDDDDDDDDDD\033[m  \n", this.getName());
												System.out.println("\033[1;32m+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\033[m");
												System.out.println("");
												break;
											case 2:
												System.out.println("\033[1;32m+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\033[m");
												System.out.printf (" \033[1;35m%s:\033[m \033[1;37mCan\033[m \033[1;31mYOU\033[m \033[1;37mplay with me ?\033[m  \n", this.getName());
												System.out.println("\033[1;32m+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\033[m");
												System.out.println("");
												break;
											default:
												break;
										}
										return a;
									}
									else {
										System.out.println("");
										System.out.println("Error : Out of range!!!");
										System.out.println("Press Enter to continue...");
										try {
											System.in.read();
										}
										catch(Exception e) {
										}
										break;
									}
								}
							}
						}
						else {
							System.out.println("");
							System.out.println("Error : Pet doesn't exist!!");
							System.out.println("Press Enter to continue...");
							try {
								System.in.read();
							}
							catch(Exception e) {
							}
						}
					}
					break;
				case 2:
					if(mp < 30) {
						System.out.println("");
						System.out.println("MP not enough !!!");
						System.out.println("Press Enter to continue...");
						try {
							System.in.read();
						}
						catch(Exception e) {
						}
					}
					else {
						System.out.println("\033[1;33mWhich pet you'd like to attack ?\033[m");
						System.out.println("");
						System.out.printf("\033[1;37mPlease enter the pet index : \033[m");
						tmp = scanner.next();
						scanner.nextLine();
						System.out.println("");
						if(isInteger(tmp)) {
							des_pet = Integer.parseInt(tmp);
						}
						else {
							System.out.println("");
							System.out.println("Error Input!!!");
							System.out.println("Press Enter to continue...");
							try {
								System.in.read();
							}
							catch(Exception e) {
							}
							break;
						}
						if(des_pet >= 0 && des_pet < arena_f.petArray.length) {
							if(arena_f.petArray[des_pet] == null) {
								System.out.println("");
								System.out.println("Error : This pet is already dead!!!");
								System.out.println("Press Enter to continue...");
								try {
									System.in.read();
								}
								catch(Exception e) {
								}
							}
							else {
								p = arena_f.petArray[des_pet];
								des_coo = arena.getPosition(p);
								if(des_coo == null) {
									System.out.println("");
									System.out.println("Error : This pet is already dead!!!");
									System.out.println("Press Enter to continue...");
									try {
										System.in.read();
									}
									catch(Exception e) {
									}
								}
								else {
									my_coo = arena.getPosition(this);
									if(des_coo.x - my_coo.x < 0) {
										res_x = (des_coo.x - my_coo.x) * (-1);
									}
									else {
										res_x = des_coo.x - my_coo.x;
									}
									if(des_coo.y - my_coo.y < 0) {
										res_y = (des_coo.y - my_coo.y) * (-1);
									}
									else {
										res_y = des_coo.y - my_coo.y;
									}
									res = res_x + res_y;
									if(res <= 2) {
										this.setMP(mp - 30);
										a.skill = skill_3;
										a.dest = p;
										System.out.println("");
										System.out.println("");
										System.out.println("");
										rrr = ran.nextInt(3);
										switch(rrr) {
											case 0:
												System.out.println("\033[1;32m+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\033[m");
												System.out.printf (" \033[1;35m%s:\033[m \033[1;37mHahahahaha !!!!\033[m \033[1;31mYOU\033[m \033[1;37mCompile Error!!!!\033[m  \n", this.getName());
												System.out.println("\033[1;32m+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\033[m");
												System.out.println("");
												break;
											case 1:
												System.out.println("\033[1;32m+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\033[m");
												System.out.printf (" \033[1;35m%s:\033[m \033[1;37mXDDDDDDDDDDDDDDDDDDD\033[m  \n", this.getName());
												System.out.println("\033[1;32m+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\033[m");
												System.out.println("");
												break;
											case 2:
												System.out.println("\033[1;32m+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\033[m");
												System.out.printf (" \033[1;35m%s:\033[m \033[1;37mCome on. Play with me!!!!!\033[m  \n", this.getName());
												System.out.println("\033[1;32m+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++\033[m");
												System.out.println("");
												break;
											default:
												break;
										}
										return a;
									}
									else {
										System.out.println("");
										System.out.println("Error : Out of range!!!");
										System.out.println("Press Enter to continue...");
										try {
											System.in.read();
										}
										catch(Exception e) {
										}
										break;
									}
								}
							}
						}
						else {
							System.out.println("");
							System.out.println("Error : Pet doesn't exist!!");
							System.out.println("Press Enter to continue...");
							try {
								System.in.read();
							}
							catch(Exception e) {
							}
						}
					}
					break;
				case 3:
					a.skill = skill_4;
					a.dest = this;
					return a;
				default:
					System.out.println("");
					System.out.println("Error Skill Command!!!");
					System.out.println("Press Enter to continue...");
					try {
						System.in.read();
					}
					catch(Exception e) {
					}
					break;
			}
		}
	}
	public boolean isInteger(String s) {
		try {
			Integer.parseInt(s);
		}
		catch(NumberFormatException e) {
			return false;
		}
		// only got here if we didn't return false
		return true;
	}
	protected POOCoordinate move(POOArena arena) {
		Scanner scanner = new Scanner(System.in);
		String tmp1, tmp2;
		int des_x, des_y;
		POOCoo now = new POOCoo();
		tmp1 = scanner.next();
		tmp2 = scanner.next();
		scanner.nextLine();
		if(isInteger(tmp1) && isInteger(tmp2)) {
			now.x = Integer.parseInt(tmp1);
			now.y = Integer.parseInt(tmp2);
		}
		else {
			return null;
		}
		return now;
	}
}
